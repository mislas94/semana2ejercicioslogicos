function arrayOfMultiples(num, length) {
  let result = [];
  for (let i = 0; i < length; i++) {
    const numMult = num * (i + 1);
    result.push(numMult);
  }
  return result;
}

module.exports = arrayOfMultiples;
