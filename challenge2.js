function divisibleByLeft(n) {
  //Implementación
  let result = [];
  const numArr = n.toString().split('');
  if (numArr.length > 0) {
    for (let i = 0; i < numArr.length; i++) {
      if (Number.isInteger(numArr[i] / numArr[i - 1])) {
        result.push(true);
      } else {
        result.push(false);
      }
    }
  }

  return result;
}

module.exports = divisibleByLeft;
